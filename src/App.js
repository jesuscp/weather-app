import React, { Component } from 'react';
//import logo from './logo.svg';
import './App.css';
import WeatherLocation from './components/WeatherLocation';
//var Request = require("request");


/*Request.get("http://localhost:5000/items", (error, response, body) => {
    if(error) {
        return console.dir(error);
    }
    console.log(JSON.parse(body));
});*/

class App extends Component {
  render() {
    return (
      <div className="App">
        <header className="App-header">
          <WeatherLocation />
        </header>
      </div>
    );
  }
}

export default App;
