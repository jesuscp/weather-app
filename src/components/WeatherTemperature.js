import React from 'react';
import WeatherIcons from 'react-weathericons';
import PropTypes from 'prop-types';

const getstateToIconName = (weatherState) => {
    switch (weatherState) {
        case "cloud":
            return "cloud";
        case "sun":
            return "day-sunny";
        case "windy":
            return "windy";
        default:
            return "day-sunny";
    }
};

const getWeatherIcon = (weatherState) => {
    return (<WeatherIcons className="wicon" name={getstateToIconName(weatherState)} size="4x" />);
};

const WeatherTemperature = ({ temperature, weatherState}) => (
    <div className="weatherTemperatureCont">
        {getWeatherIcon(weatherState)}
        <span className="temperature">{ `${temperature} C°` }</span>
    </div>
);

WeatherTemperature.propTypes = {
    temperature: PropTypes.number.isRequired,
    weatherState: PropTypes.string.isRequired
}

export default WeatherTemperature;