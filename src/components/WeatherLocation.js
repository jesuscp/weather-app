import React, { Component } from 'react';
import Location from './Location';
import WeatherData from './WeatherData';
  
const _temp = null; // 55;
const _url_api = "https://api.apixu.com/v1/current.json?key=84bd023b268f40ffaa420648182608&q=Paris"; // "https://samples.openweathermap.org/data/2.5/weather?q=London,uk&appid=b6907d289e10d714a6e88b30761fae22";
//const _url_api = "https://samples.openweathermap.org/data/2.5/weather?q=London,uk&appid=b6907d289e10d714a6e88b30761fae22";

class WeatherLocation extends Component{ 

    constructor(){
        super();

        this.state = {
            temp: _temp,
            city: "Rio de Janeiro",
            icon: "cloud"
        }
        //una sola vez
        console.log("constructor");
    }

    
    componentWillMount() {

        this.handlerUpdateClick();

        //una sola vez
        console.log("componentWillMount");
    }

    componentDidMount() {
        //una sola vez al final
        console.log("componentDidMount");
    }
    
    /* se lanzan ante cualquier cambio en el componente (render) */

    componentWillUpdate() {
        //una sola vez luego de modificar el componente
        console.log("componentWillUpdate");
    }
    
    componentDidUpdate() {
        //una sola vez al final del render
        console.log("componentDidUpdate");
    }
    
    
    handlerUpdateClick = () => {
  
        this.setState({
            temp: null, //33,
            city: "Lima",
            icon: "sun"
        })

        fetch(_url_api)
        .then(response => response.json())
        .then(json => {
            console.log(json);
            this.setState({
                temp: json.current.humidity,
                city: json.location.name
            })
        })
	 
       
    }

    render () { 
        console.log("render");
        const { temp, city, icon } = this.state;

        return (
            <div>
                <Location city={city} />
                { temp ? <WeatherData temp={temp} icon={icon} /> : "Cargando..." }
                <button onClick={this.handlerUpdateClick}>Actualizar</button>
            </div>
        );
    };
}
 
export default WeatherLocation;