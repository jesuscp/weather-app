import React from 'react';
import WeatherExtraInfo from './WeatherExtraInfo';
import WeatherTemperature from './WeatherTemperature';
import '../assets/style.css';

const WeatherData = ({ temp, icon }) => (
    <div className="weatherDataCont">
        <WeatherTemperature temperature={temp} weatherState={icon} />
        <WeatherExtraInfo humidity={80} wind={"10 m/s"} />
    </div>
);

export default WeatherData;