import React from 'react';
import PropTypes from 'prop-types';
 

const Location = ({ city }) => {
    //console.log(props);
    //debugger;
    //const { city } = props;

    return(
    <div>
        <h1>{city ? city : "No hay ciudad"}</h1>
    </div>
    );
};
     
Location.propTypes = {
    city: PropTypes.string.isRequired,
}
export default Location;